// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace IssueReporter
{
    [Register ("ReportController")]
    partial class ReportController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ChoosePhoto { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView image { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ProblemText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton TakePhoto { get; set; }

        [Action ("ChoosePhoto_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ChoosePhoto_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (ChoosePhoto != null) {
                ChoosePhoto.Dispose ();
                ChoosePhoto = null;
            }

            if (image != null) {
                image.Dispose ();
                image = null;
            }

            if (ProblemText != null) {
                ProblemText.Dispose ();
                ProblemText = null;
            }

            if (TakePhoto != null) {
                TakePhoto.Dispose ();
                TakePhoto = null;
            }
        }
    }
}