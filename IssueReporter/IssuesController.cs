using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace IssueReporter
{
    public partial class IssuesController : UIViewController
    {
        public IssuesController (IntPtr handle) : base (handle)
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            List<string> data = new List<string>() { "ISSUE 1", "ISSUE 2" };

            ListIssues.Source = new IssuesListViewSource(data);
            
        }
    }
}